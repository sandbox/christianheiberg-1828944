<?php
/**
 * @file
 * Plugin for node retreival.
 */

function webservice_app_node_init() {
  webservice_app_foo_bar();
}
function webservice_app_node_foo_bar() {
  mobile_send_status_response('OK', 'FOO_BAR'); 
}
/**
 * webservice_app_node_get_node_nocheck return any given node in json.
 * this function is only for testing purpose
 * supplied arguments are:
 * nid: by calling [sitename]/services/node/get/node/nocheck/[nid], you will get the current node
 * vid: by calling [sitename]/services/node/get/node/nocheck/[nid]/[vid], you will get any given version of a node
 */
function webservice_app_node_get_node_nocheck($nid = NULL, $vid = NULL) {
  $array['nid'] = $nid;
  $vid ? $array['vid'] = $vid : null;
  $node = node_load($array);
  var_dump($node);
  $return = array('node' => $node);
  //mobile_send_status_response('OK', $return);
}

function webservice_app_node_get_node() {
  $headers = mobile_request_headers();
  $header_key = variable_get('webservice_settings_headerkey', 'header_key');
  if (array_key_exists($header_key, $headers)) {
    $json_elements = json_decode(rawurldecode($headers[$header_key]), TRUE);
    $node = node_load(array('nid' => $json_elements['nid']));
    $return = array('node' => $node);
    mobile_send_status_response('OK', $return);
  }
  mobile_send_status_response('ERROR', 'E_NOT_A_QUALIFIED_MOBILE_DEVICE');
}