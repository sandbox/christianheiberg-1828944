<?php
/**
 * @file
 * Plugin for user object retreival.
 */
 
function webservice_app_user_init() {
  webservice_app_foo_bar();
}

function webservice_app_user_foo_bar() {
  mobile_send_status_response('OK', 'FOO_BAR'); 
}
/**
 * webservice_app_user_get_user_nocheck return any given user object in json.
 * this function is only for testing purpose
 * supplied argument is:
 * uid: by calling [sitename]/services/user/get/user/nocheck/[uid], you will get the user object of that current user.
 */
function webservice_app_user_get_user_nocheck($uid) {
  $user = user_load(array('uid' => $uid));
  $return = array('user' => $user);
  mobile_send_status_response('OK', $return);
}

function webservice_app_user_get_user() {
  $headers = mobile_request_headers();
  $header_key = variable_get('webservice_settings_headerkey', 'header_key');
  if (array_key_exists($header_key, $headers)) {
    $json_elements = json_decode(rawurldecode($headers[$header_key]), TRUE);
    $user = user_load(array('uid' => $json_elements['uid']));
    $return = array('user' => $user);
    mobile_send_status_response('OK', $return);
  }
  mobile_send_status_response('ERROR', 'E_NOT_A_QUALIFIED_MOBILE_DEVICE');
}