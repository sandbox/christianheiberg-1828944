By Christian Heiberg.
mail: chh@fbr.dk

====================== About =====================

The webservice_app module lets you create json data output from drupal to any given mobile device,
In any given output format. That is, json, xml, other (not a format ;))

It is meant to be the glue between an app and drupal.
The philosophy of this module is to make a module with a standardized function calling methodic.
The module is plug-in based and thus highly configurable (programming examples are included).

================= Installation ===================
Well, you know the drill

================= Configuration ==================

This module comes with a few configuration points, which can be left out in some cases.
To access the settings menu, you have to get permission to "administer site configuration".
The configuration men can be found here: 'admin/settings/webservice_app'
1. First level of the URL path will, if not set, be '[sitename]/services/', or if set, '[sitename]/[your choice]/'
2. This is an encryption key, which can be used between drupal and your app. The app has to encrypt with the same key.
   There will be an example later.
3. Name of header key in which the arguments will pass through to your plug-in.

========= well, how does it all works? ==========

Well, for starters, it doesn't work, out of the box.
This module needs its own plug-in, and it comes with two, for testing, but they hardly do your job???
It�s strictly for testing.

Getting things to work is easy.
Everything around this module is controlled by the calling URL and by choice, what has been thrown in to the header, by the app.
Q. What does that mean?
A. It means, that by calling [sitename]/services/node, you are calling the NODE plug-in (with no defined parameters, though)
By calling [sitename]/services/node/get/node, you are calling the plug-in node, and it's function webservice_app_node_get_node().
In this case, it will lead to a verbal error response, telling it's not a mobile device. I'm getting back to this.

Q. How do I make a plug-in?
A. it's easy. You only need to follow a naming convention as described below.
   All plug-in are placed inside the services folder.
   Let's say, you like to make a plug-in, that generates a lookup in your taxonomy and returns something to your app.
   Start out with creating a file called tax.inc and place it inside the services folder.
   The function naming goes like this: webservice_app_[plugin name]_init().
   All the following function follows the same naming idea. Let's say you are calling [sitename]/services/tax/lookup/33
   This will call webservice_app_tax_lookup(33). All ints are converted to arguments and send to the function in that order they are placed in the URL.
   There is no security check here, so I will advise you to do all arguments through the http header, as described below.
   
It is possible when building an app (in most cases), to extend the http header with an extra key.
The header may and may not contain a great number of keys, containing all sorts of data, from session cookies, timestamps and other needful things.
Let's say you build an app with its own header key, and you call it 'my-own-special-key'
Every time you like your app to communicate with drupal, you create an array like this: array('tid' => 33), and the you serialize it and give it as value, to 'my-own-special-key'
The you call [sitemane]/services/tax/lookup. The argument (33) will be nested inside the headerkey and will be read directly by webservice_app_tax_lookup(). (remember to unserialize!!!)

Q. Now how about that encryption key?
A. This is not necessary, but I will advise you to use it, or similar functionality, when working with sensitive data like user verification.
   The encryption key is a paraphrase that has to be nested inside the app as well as drupal.
   You then encrypt all your arguments inside 'my-own-special-key' alongside the encryption paraphrase. It�s done with md5
   You then send it all unencrypted alongside an md5 version of it all + the encryption paraphrase.
   Drupal can the check up on the md5 encryption by trying, doing the same encryption
   To show the logics in a more visual manner, follow this example.
   $encrypete_stuff = md5(md5($uid ) . md5('paraphrase')).
   http-> header-> my-own-special-key-> array( uid => 1, md5key => $encrypete_stuff).
   Now, your drupal box will treat that header in the same manner, and compares the md5key with its own result, and if there is a hit, voila. it validates.

================== Future use ================

This module, with the right plugin, can be used to pull pages from other drupal sited and placed inside you own content.
In that case, you need a corresponding module in your installation, to pull data in the same manner as described above.